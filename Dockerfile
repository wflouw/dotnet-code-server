FROM codercom/code-server:latest

RUN sudo apt upgrade -y
RUN sudo apt update
RUN sudo apt install wget
RUN wget https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN sudo dpkg -i packages-microsoft-prod.deb
RUN rm packages-microsoft-prod.deb
RUN sudo apt-get install -y apt-transport-https
RUN sudo apt-get update
RUN sudo apt-get install -y dotnet-sdk-6.0
RUN code-server --install-extension muhammad-sammy.csharp
RUN code-server --install-extension k--kato.docomment
