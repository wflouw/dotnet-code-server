docker build --network=host -t dotnet-code-server:local .

docker run --rm -it --name code-server --network=host \
  -p 8080:8080 \
  -v "$HOME/.config:/home/coder/.config" \
  -v "$PWD:/home/coder/project" \
  -u "$(id -u):$(id -g)" \
  -e "DOCKER_USER=$USER" \
  dotnet-code-server:local



ssh -fN -L 8080:192.168.178.61:8080 raspi

sudo netstat -tulpn | grep LISTEN

docker exec -it code-server cat ~/.config/code-server/config.yaml